//
//  ExpandableTableViewCell.swift
//  Expanding Table View Cells
//
//  Created by Momen Zarad on 09/02/2022.
//

import UIKit

class ExpandingTableViewCellContent {
    var title: String?
    var subtitle: String?
    var expanded: Bool

    init(title: String, subtitle: String) {
        self.title = title
        self.subtitle = subtitle
        self.expanded = false
    }
}

class ExpandableTableViewCell: UITableViewCell ,ExpandableCell{

    @IBOutlet weak var questionHeadLabel: UILabel!
    @IBOutlet weak var questionBodyLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    func displayQAndA(content: ExpandingTableViewCellContent) {
        self.questionHeadLabel.text = content.title
        self.questionBodyLabel.text = content.expanded ? content.subtitle : ""
        if content.expanded {
            arrowImage.transform = CGAffineTransform(rotationAngle: .pi)
            arrowImage.image = UIImage(named: "expand")
        }
        else{
            arrowImage.transform = CGAffineTransform(rotationAngle: 0)
            arrowImage.image = UIImage(named: "expand")

        }
    }
    func changeExpandArrow(expandArrow: String) {
        arrowImage.transform = CGAffineTransform(rotationAngle: .pi)

        UIView.animate(withDuration: 0.2, animations: {
            self.arrowImage.transform = CGAffineTransform(rotationAngle: 0)
        }) { (finished) in
        }
    }
    
    func changeCollapseArrow(collapseArrow: String) {
        arrowImage.transform = CGAffineTransform(rotationAngle: 0)

        UIView.animate(withDuration: 0.2, animations: {
            self.arrowImage.transform = CGAffineTransform(rotationAngle: .pi)
        }) { (finished) in
        }
    }
    
    func rotateImage(image : String){
        arrowImage.transform = arrowImage.transform.rotated(by: .pi)
    }

}

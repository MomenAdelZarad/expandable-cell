//
//  ViewController.swift
//  Expanding Table View Cells
//
//  Created by Momen Zarad on 09/02/2022.
//

import UIKit

protocol ExpandableCell {
    func displayQAndA(content : ExpandingTableViewCellContent)
    func changeExpandArrow(expandArrow : String)
    func changeCollapseArrow(collapseArrow : String)
    func rotateImage(image:String)
}

class ViewController: UIViewController {
    
    @IBOutlet weak var faqTableView: UITableView!
    
        var headerDesc = "Grand Central Dispatch (GCD) is a low-level API for managing concurrent operations. It can help you improve your app’s responsiveness by deferring computationally expensive tasks to the background. It’s an easier concurrency model to work with than locks and threads.this two-part Grand Central Dispatch tutorial, you’ll learn the ins and outs of GCD and its Swifty API. This first part will explain what GCD does and showcase several basic GCD functions. In the second part, you’ll learn about some advanced functions GCD has to offer."
    
        var datasource = [ExpandingTableViewCellContent(title: "Question Headline.",
                                                     subtitle: "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas sed diam eget risus varius blandbus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget madsadasdasdasdasdadasdasdasdasdbus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget madsadasdasdasdasdadasdasdasdasdbus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget madsadasdasdasdasdadasdasdasdasdbus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget madsadasdasdasdasdadasdasdasdasdit sit amet non magna."),
                       ExpandingTableViewCellContent(title: "Question Headline.",
                                                     subtitle: "Etiam porta sem malesuada magna mollis euismod. Nullam quis risus urna mollis ornare vel eu leo."),
                       ExpandingTableViewCellContent(title: "Question Headline.",
                                                     subtitle: "Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget madsadasdasdasdasdadasdasdasdasdetus.")]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        faqTableView.delegate = self
        faqTableView.dataSource = self
    }


}
extension ViewController : UITableViewDelegate , UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let expandableCell = faqTableView.dequeueReusableCell(withIdentifier: "ExpandableCell", for : indexPath) as! ExpandableTableViewCell
        expandableCell.displayQAndA(content: datasource[indexPath.row])
            return expandableCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let expandableCell = tableView.cellForRow(at: indexPath)as! ExpandableCell
        datasource[indexPath.row].expanded = !datasource[indexPath.row].expanded
                if datasource[indexPath.row].expanded
                {
                    expandableCell.changeExpandArrow(expandArrow: "expand")
                }else{
                    expandableCell.changeCollapseArrow(collapseArrow: "expand")
                }
        let section = IndexSet(integer: indexPath.section)
        tableView.reloadSections(section, with: .fade)
    }

}
